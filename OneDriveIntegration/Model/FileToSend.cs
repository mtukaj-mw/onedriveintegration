﻿using Newtonsoft.Json;

namespace OneDriveIntegration.Model
{
    public class FileToSend
    {
        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "fileUrl")]
        public string FileUrl { get; set; }
    }
}