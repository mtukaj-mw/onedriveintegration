﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace OneDriveIntegration.Controllers
{
    public class FileController : UmbracoApiController
    {
        // GET: File
        [System.Web.Http.HttpGet]
        [HttpHead]
        public HttpResponseMessage File(string fileName)
        {
            var filePath = Path.Combine(ConfigurationManager.AppSettings["fileUploadsPath"], fileName);
            var contentType = MimeMapping.GetMimeMapping(filePath);
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            return response;
        }
    }
}