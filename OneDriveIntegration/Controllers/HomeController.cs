﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using OneDriveIntegration.Model;
using Umbraco.Web.Mvc;

namespace OneDriveIntegration.Controllers
{
    public class HomeController : SurfaceController
    {
        private const string ClientId = "14460bf5-eea0-441e-9c47-39d7a1f9975f";
        private const string Secret = "rgarRY55?pasQZOOS074-+{";
        private const string CallbackUri = "http://localhost:63309/";

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        
        public async Task<ActionResult> Send(HttpPostedFileBase file)
        {
            if (!this.Request.ContentType.Contains("multipart/form-data") && file == null)
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var fileName = BuildFileName(file.FileName);

            try
            {
                SaveFile(file, fileName);

                var response = await SendFileToOneDrive(fileName, Request.Url.Host).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    ViewBag.Message = "File uploaded to OneDrive successfully";
                    return View();
                }
                else
                {
                    ViewBag.Message = $"Cannot upload file to OneDrive";
                    return View();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                ViewBag.Message = $"Something went wrong - {ex.Message}";
                return View();
            }
            finally
            {
                RemoveFile(fileName);
            }
        }

        private async Task<HttpResponseMessage> SendFileToOneDrive(string fileName, string hostName)
        {
            var logicAppUrl =
                @"https://prod-89.westeurope.logic.azure.com/workflows/ff75ddf098814636905d13ab1b63beab/triggers/manual/paths/invoke/uploadFile?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=ino6pDE584gvm3bX1AqHMfXw_z6H0YurhBdTHZ6qtsI";

            using (HttpClient client = new HttpClient())
            {
                client.Timeout = new TimeSpan(0, 0, 10, 0);
                var objectToSend = new FileToSend
                {
                    FileName = fileName,
                    FileUrl = $"http://{hostName}{ConfigurationManager.AppSettings["getFileRoute"]}{fileName}"
                };

                var response = await client.PostAsJsonAsync(logicAppUrl, objectToSend);
                return response;
            }
        }

        private void SaveFile(HttpPostedFileBase file, string fileName)
        {
            string targetPath = ConfigurationManager.AppSettings["fileUploadsPath"];

            var fileBytes = new byte[file.ContentLength];

            file.InputStream.ReadAsync(fileBytes, 0, file.ContentLength);

            file.SaveAs(Path.Combine(targetPath, fileName));
        }

        private void RemoveFile(string fileName)
        {
            try
            {
                var filePath = $"{ConfigurationManager.AppSettings["fileUploadsPath"]}\\{fileName}";
                System.IO.File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private string BuildFileName(string orginalName)
        {
            return $"{DateTime.UtcNow.Ticks}_{orginalName}";
        }
}
}